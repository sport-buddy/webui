import React, {useContext, useState} from 'react'
import '../css/Calendar.css'
import {UserProfile} from "./UserProfile";
import {Button, PopUpDiv} from "./MainElements";
import {useTransform} from "../hooks/transform.hook";
import {useHttp} from "../hooks/http.hook";
import {useSuccess} from "../hooks/success.hook";
import {AuthContext} from "../context/AuthContext";

const Row = ({content}) => { //name, content[] - массив от 0 до 6, itemClass - класс общий для всех элементов, rawClass
    return (
        <section className="calendar__week">
            {content}
        </section>
    )
}


export const Calendar = ({formTraining = [], token, curMonth, currWeekDay, currDate}) => {
    const weekDaysFull = {
        1: "Понедельник",
        2: "Вторник",
        3: "Среда",
        4: "Четверг",
        5: "Пятница",
        6: "Суббота",
        0: "Воскресенье",
    }
    const {successStatusNotification} = useSuccess()
    const {request} = useHttp()
    const {formTransform, getFromConfig} = useTransform()
    const auth = useContext(AuthContext)
    const [selectedDate, newSelectedDate] = useState(currDate)

    // let trainings = [];

    const dayOfWeekReduce = {
        Sun: 0,
        Mon: 1,
        Tue: 2,
        Wed: 3,
        Thu: 4,
        Fri: 5,
        Sat: 6
    }

    const TrainingsSidebar = (t = []) => { //name, content[] - массив от 0 до 6, itemClass - класс общий для всех элементов, rawClass
        return (
            <aside className="calendar__sidebar">
                <h2 className="sidebar__heading">{weekDaysFull[selectedDate.getDay()]}<br/>{getMonth(selectedDate.getMonth())} {selectedDate.getDate()}
                </h2>
                <ul className="sidebar__list">
                    {t}
                </ul>
            </aside>
        )
    }

    const userExample = {
        name: 'string',
        second_name: 'string'
    }

    const itemExample = {
        comment: 'string',
        fee: 'string',
        id_level: 'string',
        location: 'string',
        meet_date: 'UI_date',
        owner: 'int', //владелец, админ тренировки
        participants_ids: 'another',
        sport: 'string',
        training_duration: 'Bk_time'
    }

    const ITEM_REDUCE = {
        "owner": "Организатор тренировки",
        "meet_date": "Дата проведения",
        "training_duration": "Длительность тренировки",
        "comment": "Коментарий",
        "fee": "Стоимость участия",
        "id_level": "Уровень подготовки",
        "location": "Локация",
        "participants_ids": "Участники",
        "sport": "Спорт",
    }

    async function deleteBtnHandler(event, ownerMod) {  // обработчик кнопки "сохранить изменения"
        try {
            if (ownerMod) {
                const data = await request(`/training/${event.target.id}`, 'DELETE', {}, {
                    'X-Auth-Token': `${auth.token}`
                })
                await successStatusNotification(data.status)
            } else {
                const data = await request(`/user/training/${event.target.id}`, 'DELETE', {}, {
                    'X-Auth-Token': `${auth.token}`
                })
                await successStatusNotification(data.status)
            }
        } catch (e) {
            console.log('Ошибка при запросе', e)
        }
        document.location.reload(); //наши любимые костыли
    }


    const parseItem = (item, reduce, itemClass = "") => {  // расшифровать из сокращений ITEM_REDUCE ответы
        const res = []
        for (let key in reduce) {
            if (item.hasOwnProperty(key) && typeof (reduce[key]) == 'string' && item[key] !== '') {
                let parseRes
                let temp_res
                if (key === 'owner') {
                    parseRes = <UserProfile id={item[key]}/>
                } else if (key === 'participants_ids') {
                    parseRes = []
                    for (let i of item[key])
                        parseRes.push(<><UserProfile id={i}/> </>)
                } else {
                    parseRes = getFromConfig(item[key], itemExample[key])
                }
                temp_res = <div className={itemClass} key={key}> {reduce[key]}: {parseRes}</div>
                res.push(temp_res)
            }
        }
        return res
    }

    const parseStartTime = (item) => {  // если показывать время начала - время конца, там много косяков и доебок с граничными минутами, часами и даже датами (тренировка с 31.12.21 23:00 по 1.01.21 1:00)
        const res = item.meet_date.slice(11, 16)
        return res
    }

    const getTrainingsByDate = (dateIn) => {
        let res = []
        let tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        let dateISO = (new Date(dateIn - tzoffset)).toISOString().slice(0, 10)

        for (let item of formTraining) {
            try {
                let itemDateISO = item.meet_date.slice(0, 10)

                if (itemDateISO === dateISO) {
                    res.push(item)
                }
            } catch (e) {
            }
        }
        return res
    }

    const prepareTrainings = (dateIn) => {
        let res = getTrainingsByDate(dateIn)
        if (res.length > 0) {
            let resDOM = []
            for (let item of res) {

                let ownerMod = item['owner'] === auth.userId

                let parsedItem = parseItem(item, ITEM_REDUCE)
                //let parsedPreItem = parseItem(item,PREITEM_REDUCE, "pre-item")
                let DOMItem =
                    <div className="training" key={item.id_training}>
                        <PopUpDiv
                            id={item.id_training + 'popUp'}
                            content={
                                <li className="sidebar__list-item sidebar__list-item--complete">
                                    <div
                                        className="list-item__time">{parseStartTime(item)}
                                    </div>
                                    {item.sport}
                                </li>
                            }
                        >
                            {parsedItem}
                            <>
                                {
                                    !ownerMod &&
                                    <Button
                                        typeName='button'
                                        id={item.id_training}
                                        className="btn"
                                        innerText="Отменить участие"
                                        onClick={(event) => deleteBtnHandler(event, ownerMod)}
                                    />
                                }
                                {
                                    ownerMod &&
                                    <>
                                        <Button
                                            typeName='button'
                                            id={item.id_training}
                                            className="btn"
                                            innerText="Удалить тренировку"
                                            onClick={(event) => deleteBtnHandler(event, ownerMod)}
                                        />
                                        <Button
                                            typeName='link'
                                            href={'/createGroup/' + item.id_training}
                                            className="btn"
                                            innerText="Редактировать тренировку"
                                        />
                                    </>
                                }
                            </>
                        </PopUpDiv>
                    </div>
                resDOM.push(DOMItem)
            }
            return resDOM
        } else
            return null

    }

    const countFirstDay = (dateIn) => { //передать дату просматриваемого месяца
        let date = new Date(dateIn.getTime())
        date.setDate(1)
        if (date.getDay() === 0) {
            return 6
        } else {
            return date.getDay() - 1
        }
    }

    const countDaysInMonth = (year, month) => {
        return new Date(year, month + 1, 0).getDate()
    }

    function chooseDate(date) {
        newSelectedDate(date)
        // const el = event.target;
        // el.setAttribute('style', 'color: blue');
    }

    function countTrainings(date) {
        let l = getTrainingsByDate(date).length
        if (l>0){
            return l
        } else {
            return ""
        }

    }

    const getDay = (curDate, itemDate) => {
        if (curDate.getTime() === itemDate.getTime()) {
            return <div className="calendar__day today" onClick={chooseDate.bind(this, new Date(itemDate))}>
                <span className="calendar__date">{String(itemDate.getDate())}</span>
                <span className="calendar__task">{countTrainings(itemDate)}</span>
            </div>
        } else if ((curMonth === itemDate.getMonth())) {
            return <div className="calendar__day" onClick={chooseDate.bind(this, new Date(itemDate))}>
                <span className="calendar__date">{String(itemDate.getDate())}</span>
                <span className="calendar__task">{countTrainings(itemDate)}</span>
            </div>
        } else {
            return <div className="calendar__day inactive">
                <span className="calendar__date">  {String(itemDate.getDate())}</span>
            </div>
        }
    }

    const countMonth = () => {
        console.log(curMonth)
        const date = new Date()
        const curDate = new Date() //текущая дата
        date.setMonth(curMonth)
        console.log('date | mont | date.mont>>>', date, curMonth, date.getMonth())
        const itemDate = date //изменяющаяся дата чтобы пробежжаться по месяцу
        itemDate.setDate(-1 * countFirstDay(date)) //set item date on first day in first week in month

        const myMonth = []
        let week = []
        for (let iter = 0; iter < 42; iter++) {
            itemDate.setDate(itemDate.getDate() + 1)
            week.push(getDay(curDate, itemDate))
            if (iter % 7 === 6) {
                myMonth.push(<Row name="week" content={week} itemClass="calendar__days"/>)
                week = []
                let nextDay = new Date()
                nextDay.setMonth(itemDate.getMonth())
                nextDay.setDate(itemDate.getDate() + 1)
                if (curMonth !== itemDate.getMonth() || nextDay.getMonth() !== curMonth) break
            }
        }

        return myMonth
    }

    const month = countMonth()
    const getMonth = (month) => {  // обработчик кнопки присоединиться
        const months = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"]
        return months[month]
    }
    console.log(selectedDate)
    return (
        <div>
            {TrainingsSidebar(prepareTrainings(selectedDate))}
            <section className='calendar__days'>
                <section className="calendar__top-bar">
                    <span className="top-bar__days">Mon</span>
                    <span className="top-bar__days">Tue</span>
                    <span className="top-bar__days">Wed</span>
                    <span className="top-bar__days">Thu</span>
                    <span className="top-bar__days">Fri</span>
                    <span className="top-bar__days">Sat</span>
                    <span className="top-bar__days">Sun</span>
                </section>
                {month}
            </section>
        </div>
    )
}
