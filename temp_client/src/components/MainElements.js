import React,  { useState, useEffect} from 'react'
import {NavLink} from 'react-router-dom'
import ReactInputMask from 'react-input-mask';
import Validation, {DisabledOnErrors, ValidationInput} from 'react-livr-validation';
import '../css/MainElements.css'

const removeItemFromArray = (myArray,valueToRemove) => {
    let myIndex = myArray.indexOf(valueToRemove);
    if (myIndex !== -1) {
        myArray.splice(myIndex, 1);
    }
}

const changeOpenStatus = (temp) => {
    superChangeStatus(temp,{'closed':'open','close':'open'})
}

const changeStatus = (target,first_value,second_value) => {
    if(target.className.indexOf(first_value)>-1){
        target.className = target.className.replace(first_value,'')
        target.className = target.className + second_value
    }
    else if(target.className.indexOf(second_value)>-1){
        target.className = target.className.replace(second_value,'')
        target.className = target.className + first_value
    }
    else return false
    return true
}

const superChangeStatus = (target,value,type= 0) => { // value: {'value1':'mirror_value1','value2':'mirror_value2'} type= 0 - 'до первой замены' | 1 - 'все замены'
    if(type === 0)
        for (let first in value){
            if(changeStatus(target,first,value[first])) break
        }
    else if(type === 1){
        for (let first in value){
            changeStatus(target,first,value[first])
        }
    }
}

export const InputField = ({
                               inputType="maskInput" || "validInput",
                               outText='',
                               innerText='',
                               isDisabled=false,
                               id='',
                               name='',
                               mask,
                               maskPlaceholder,
                               typeName='text',
                               valueName,
                               changeHandler,
                               schema,
                               data,
                               step,
                               className=''
                           }) => {
    if(inputType==="maskInput")
        return (
            <div className='input-field-wrapper' id={id}>
                <label htmlFor={name} className="out-text">{outText}</label>
                <div className={'input-field-wrap '+className}>
                    <ReactInputMask
                        maskPlaceholder={maskPlaceholder}
                        mask={mask}
                        disabled={isDisabled}
                        id={id}
                        name={name}
                        type={typeName}
                        value={valueName}
                        onChange={changeHandler}
                        placeholder={innerText}
                        step={step}
                    />
                </div>
            </div>
        )
    else if(inputType==="textArea"){
        return (
            <div className='input-field-wrapper' id={id}>
                <label htmlFor={name} className="out-text">{outText}</label>
                <div className={'input-field-wrap '+ className}>
                    <textarea
                              disabled={isDisabled}
                              id={id}
                              name={name}
                              value={valueName}
                              onChange={changeHandler}
                              placeholder={innerText}>
                    </textarea>
                </div>
            </div>
        )
    }
    else if(inputType==="validInput"){
        return (
            <div className='input-field-wrapper' id={id}>
                <label htmlFor={name} className="out-text">{outText}</label>
                <div className="input-field-wrap">
                    <Validation               //я возлагал на тебя большие надежды, но придется писать вручную
                        data={data}
                        schema={schema}
                    >
                        <ValidationInput>
                            <input
                                disabled={isDisabled}
                                id={id}
                                name={name}
                                type={typeName}
                                value={valueName}
                                onChange={changeHandler}
                                placeholder={innerText}
                                step={step}
                            />
                        </ValidationInput>
                    </Validation>
                </div>
            </div>
        )
    }
}

const ChangeableImage = (props,{src='', alt="Фото",clickHandler}) => {

    const [wasLoaded, setWasLoaded] = useState(true)

    const addOnLoadError = (ev) => {
        setWasLoaded(false)
    }

    if (wasLoaded)
        return (
            <img onError={addOnLoadError} src={src} alt={alt} onClick={clickHandler}/>
        )
    else
        return (
            props.children
        )
}

export const Image = ({src='', typeName='second',alt="Фото",clickHandler}) => {

    const addDefaultSrc = (ev) => {
        ev.target.src = 'some default image url' //подмена изображения другим изображением при незагрузке
    }

    if (typeName==='main-avatar-wrapper')
        return (
            <div className="main-avatar-wrapper"><ChangeableImage src={src} alt={alt} onClick={clickHandler}><span className="alt-text">Фото профиля</span></ChangeableImage></div>
        )
    if (typeName==='avatar-wrapper')
        return (
            <div className="avatar-wrapper"><img src={src} alt={alt} onClick={clickHandler}/></div>

        )
    if (typeName==='second')
        return (
            <div className=""><img src={src} alt={alt} onClick={clickHandler}/></div>

        )
}


//

export const Button = ({innerText='',isDisabled=false,id='',className='', typeName='button',onClick,href=''}) => {


    if (typeName === 'button'){
        return (
            <button
                id={id}
                className={className}
                disabled={isDisabled}
                onClick={onClick}
            >
                {innerText}
            </button>
        )
    }
    else if (typeName==='link'){
        return (
            <NavLink
                to={href}
                className={className}
            >
                {innerText}
            </NavLink>
        )
    }
}

export const PopUpDiv = (props) => {

    let [visibilityMode,setVisibilityMode] = useState(false)

    const closeHandler = () => {
        let temp = document.getElementById(props.id)
        changeOpenStatus(temp)
    }

    const openHandler = () => {
        let temp = document.getElementById(props.id)
        changeOpenStatus(temp)
    }

    return (
        <>
            <div className="" onClick={openHandler}>
                {props.content}
            </div>
            <div className="pop-up-wrapper closed" id={props.id}>
                <div className="pu-close-button" onClick={closeHandler}>
                    [X]
                </div>
                <div className="pu-main-div">
                    <div className="pu-content-wrapper">
                        {props.children}
                    </div>
                </div>
            </div>
        </>
    )
}

const  PartSelectField = ({options,innerText,name,value,changeHandler,isDisabled}) => {
    const optionsList = options.map((text, index) => {
        return <option key={index}>{text}</option>;
    })

    return (
        <div className="input-field-wrap">
            <select
                name={name}
                value={value}
                onChange={changeHandler}
                placeholder={innerText}
                disabled={isDisabled}
            >
                {optionsList}
            </select>
        </div>
    )
}

export const  MultiSelectField = ({options,innerText,name,outText,value,isDisabled = false,onChange}) => {

    const changeHandler = event => {
        if (!isDisabled){
            if (event.target.checked)
                value.push(event.target.value)
            else
                removeItemFromArray(value,event.target.value)
            const changeTarget={name,value}
            if(!!onChange) onChange(changeTarget)
        }
    }

    const clearHandler = () => {
        if(!isDisabled){
            let arr = document.getElementsByName(name+'-checkbox')
            for(let key in arr){
                arr[key].checked = false
                if(parseInt(key)===arr.length-1)
                    break
            }
            const changeTarget={name:name,value:[]}
            if(!!onChange) onChange(changeTarget)
        }
    }

    const sweepHandler = () => {
        let temp = document.getElementById(name+'-selector')
        changeOpenStatus(temp)
        temp = document.getElementById(name+'-wrapper')
        superChangeStatus(temp,{'open-wrapper':'closed-wrapper'})
    }

    const checkList = options.map((text, index) => {
        let isChecked = false
        if(value) isChecked = value.includes(text)
        let key = String(index)+'-'+name
        return (
                <div className="select-option-wrap" key={key}>
                    <input
                        disabled={isDisabled}
                        type="checkbox"
                        id={key+'-checkbox'}
                        name={name+'-checkbox'}
                        value={text}
                        onChange={changeHandler}
                        checked={isChecked}
                    />
                    <label htmlFor={key+'-checkbox'}>{text}</label>
                </div>
            )
    })

    return (
        <div className="super-wrapper">
            <div className="super-input-field-wrapper">
                <label className="out-text">{outText}</label>
                <div className="multi-select-field-wrapper closed-wrapper" id={name+'-wrapper'}>
                    <div className="multi-select-header">
                        <div className="mini-btn" onClick={sweepHandler}>
                            ^
                        </div>
                        <div className="mini-btn" onClick={clearHandler}>
                            X
                        </div>
                        {innerText}
                    </div>
                    <div className="multi-select-footer closed" id={name+'-selector'}>
                        {checkList}
                    </div>
                </div>
            </div>
        </div>
    )
}

export const  SelectField = ({options,innerText,name,outText,value,onChange,isDisabled}) => {

    const changeHandler = event => {
        if(!isDisabled){
            if(event.target.checked){
                let arr = document.getElementsByName(name+'-radio')
                for(let key in arr){
                    arr[key].checked = false
                    if(parseInt(key)===arr.length-1)
                        break
                }
                event.target.checked = !event.target.checked
                const changeTarget={name:name,value: event.target.value}
                if(!!onChange) onChange(changeTarget)
            }
            else{
                const changeTarget={name:name,value:''}
                if(!!onChange) onChange(changeTarget)
            }
        }
    }

    const clearHandler = () => {
        if(!isDisabled){
            let arr = document.getElementsByName(name+'-radio')
            for(let key in arr){
                arr[key].checked = false
                if(parseInt(key)===arr.length-1)
                    break
            }

            const changeTarget={name:name,value:''}
            if(!!onChange) onChange(changeTarget)
        }
    }

    const sweepHandler = event => {
        let temp = document.getElementById(name+'-selector')
        changeOpenStatus(temp)
        temp = document.getElementById(name+'-wrapper')
        superChangeStatus(temp,{'open-wrapper':'closed-wrapper'})
    }

    const checkList = options.map((text, index) => {
        let key = String(index)+'-'+name
        return (
            <div className="select-option-wrap" key={key}>
                <input
                    disabled={isDisabled}
                    type="radio"
                    id={key+'-radio'}
                    name={name+'-radio'}
                    value={text}
                    onChange={changeHandler}
                />
                <label htmlFor={key+'-radio'}>{text}</label>
            </div>
        )
    })

    const [headerText,setHeaderText] = useState(innerText)

    useEffect(() => {
        setHeaderText(value)
        let arr = document.getElementsByName(name+'-radio')
        for(let key in arr){
            if(arr[key].value===value)
                arr[key].checked=true
            if(parseInt(key)===arr.length-1)
                break
        }
    }, [value])

    return (
        <div className="super-wrapper">
            <div className="super-input-field-wrapper">
                <label className="out-text">{outText}</label>
                <div className="multi-select-field-wrapper closed-wrapper" id={name+'-wrapper'}>
                    <div className="multi-select-header">
                        <div className="mini-btn" onClick={sweepHandler}>
                            ^
                        </div>
                        <div className="mini-btn" onClick={clearHandler}>
                            X
                        </div>
                        {headerText || innerText}
                    </div>
                    <div className="multi-select-footer closed" id={name+'-selector'}>
                        {checkList}
                    </div>
                </div>
            </div>
        </div>
    )
/*
    return (
        <div className="input-field-wrapper">
            <label htmlFor="email">{outText}</label>
            <PartSelectField
                options = {options}
                innerText = {innerText}
                name = {name}
                outText = {outText}
                value = {value}
                changeHandler = {changeHandler}
                isDisabled = {isDisabled}
            />
        </div>
    )
 */
}