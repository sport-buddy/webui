import React from 'react'

//import '../css/NavBar.css'

export const SearchResult = ({items=[]}) => {
    if(items.length>0){
        return (
            <div className="search-result-field">
                {items}
            </div>
        )
    }

    return (
        <div className="search-result-field">
            <p className='no-search-result'>No search result</p>
        </div>
    )

}
