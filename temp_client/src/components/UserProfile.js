import React, {useState, useEffect, useCallback, useContext} from 'react'
import {AuthContext} from "../context/AuthContext";
import {useHttp} from "../hooks/http.hook";
import {useTransform} from "../hooks/transform.hook";
import {NavLink} from "react-router-dom";

export const UserProfile = ({id}) => { //name, content[] - массив от 0 до 6, itemClass - класс общий для всех элементов, rawClass
    const auth= useContext(AuthContext)
    const {request} = useHttp()
    const {formTransform,getFromConfig} = useTransform()
    const userExample = {
        name:'string',
        second_name: 'string',
        id_user : 'another'
    }
    const [user,setUser] = useState(userExample)
    const fetchProfileData = useCallback(async () => {

        try {
            const fetched = await request(`/user/profile/${id}`, 'GET', null, {
                'X-Auth-Token': `${auth.token}`
            })
            await setUser(formTransform(fetched,userExample,'strict'))
        } catch (e) {}
    }, [id, request])

    useEffect(() => {
        fetchProfileData()
    }, [fetchProfileData])

    return (
        <div className="userName">
            <NavLink to={`/profile/${id}`}> {user.name+' '+user.second_name} </NavLink>
        </div>
    )

}