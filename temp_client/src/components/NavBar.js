import React, {useContext} from 'react'
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from '../context/AuthContext'
import {Image} from './MainElements'
import '../css/NavBar.css'

export const Navbar = () => {
    const history = useHistory()
    const auth = useContext(AuthContext)

    const logoutHandler = event => {
        event.preventDefault()
        auth.logout()
        history.push('/')
    }
    return (
        // <nav className="nav-wrapper">
        //     {/*<div className="dropdown">*/}
        //     {/*    <button className="dropbtn">Выпадающее</button>*/}
        //     {/*    <div className="dropdown-content">*/}
        //     {/*        <ul id="nav-mobile" className="nav-ul">*/}
        //     {/*            <li><NavLink to="/menu">ГЛАВНОЕ МЕНЮ</NavLink></li>*/}
        //     {/*            <li><NavLink to="/calendar">КАЛЕНДАРЬ</NavLink></li>*/}
        //     {/*            <li><NavLink to="/searchGroup">НАЙТИ ТРЕНИРОВКУ</NavLink></li>*/}
        //     {/*            <li><NavLink to="/profile">ПРОФИЛЬ</NavLink></li>*/}
        //     {/*            <li><NavLink to="/map">КАРТА</NavLink></li>*/}
        //     {/*        </ul>*/}
        //     {/*    </div>*/}
        //     {/*</div>*/}
        //     {/*<div className="title nav-title">SportBuddy</div>*/}
        //     {/*/!*<Image alt="фото профиля" typeName="avatar-wrapper" src={require('../img/pngwing.png')}/>*!/*/}
        //     {/*<a className="outButton" href="/" onClick={logoutHandler}>Выйти из профиля</a>*/}
        //     {/*<a className="contactAdmin" href="/messenger">Связаться с администрацией</a>*/}
        // </nav>
        <nav className="top-menu">
            <ul className="menu-main">
                <li><a href="/map">КАРТА</a></li>
                <li><a href="/calendar">КАЛЕНДАРЬ</a></li>
                <li><a href="/searchGroup">НАЙТИ ТРЕНИРОВКУ</a></li>
                <li><a href="/profile">ПРОФИЛЬ</a></li>
                <li><a href="/" onClick={logoutHandler}>ВЫЙТИ</a></li>
            </ul>
        </nav>
    )
}
