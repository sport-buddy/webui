import {useState, useCallback} from 'react'

export const useTransform = () => {

    const SEX_REDUCE={
        'male':'муж',
        'female':'жен',
        'NaN':'не выбрано'
    }

    const LOCATION_REDUCE={
        'Avtovo':'Автово',
        'Nevskiy':'Невский Проспект',
        'Pioner':'Пионерская',
        'Gorkovskaya':'Горьковская'
    }

    const SPORT_REDUCE={
        'football':'футбол',
        'athletics':'атлетика',
        'basketball':',баскетбол',
        'running':'бег'
    }

    const LVL_REDUCE={
        '0':'Не выбран',
        '1':'Начинающий',
        '2':'Продвинутый',
        '3':'"Мастер'
    }

    const REDUCER = Object.assign({},SEX_REDUCE,SPORT_REDUCE,LVL_REDUCE,LOCATION_REDUCE)

    const getFromReducer = (reduce,prop) => {  // пробежаться по константам сокращений
        for (let key in reduce) {
            if(reduce.hasOwnProperty(key)){
                if(prop===key)
                    return reduce[key]
                if(prop===reduce[key])
                    return key
            }
        }
    }

    const getFromConfig = (inValue,exampleValue,mode='strict',reducer=REDUCER) => {  // пробежаться по константам сокращений
        let answer
        //console.log('getFromConfig about:\n>mode:',mode,'\n>inValue:',inValue,'\n>formIn[key]:',exampleValue)
        if(inValue===undefined)
            answer = inValue
        else if(exampleValue==='int')
            answer = parseInt(inValue)
        else if(exampleValue==='string')
            answer = String(inValue)
        else if(exampleValue==='bool')
            answer=!!inValue
        else if(exampleValue==='time'){
            answer=String(inValue)
            answer=answer.replace(/:/,"h")
            answer=answer.replace(/:/,"m")
            answer=answer+'s'
        }
        else if(exampleValue==='Bk_time'){
            answer=String(inValue)
            answer=answer.replace(/h/,":")
            answer=answer.replace(/m/,":")
            answer=answer.replace(/s/,'')
        }
        else if(exampleValue==='Spec_Bk_time'){
            let date = new Date()
            answer = String(inValue)
            date.setUTCHours(
                parseInt(answer.replace(/h.*/,'')),
                parseInt(answer.replace(/.*h/,'').replace(/m.*/,'')),
                parseInt(answer.replace(/.*m/,'').replace(/s.*/,''))
            )
            answer = date.toISOString().replace(/.*T/,'').replace(/\..*/,'')
        }
        else if(exampleValue==='date'){
            let date = new Date(String(inValue)+'Z');
            answer = date.toISOString();
        }
        else if(exampleValue==='UI_date'){
            let date = new Date(String(inValue));
            console.log()
            answer = date.toISOString().slice(11,16) + ' ' + date.toISOString().slice(0,10)
        }
        else if(exampleValue==='Spec_UI_date'){
            answer = inValue.slice(0,16)
        }
        else if(exampleValue==='select_string'){
            answer=getFromReducer(reducer,String(inValue))
        }
        else if(exampleValue==='select_int'){
            answer=parseInt(getFromReducer(reducer,String(inValue)))
        }
        else if(exampleValue==='array_string'){
            let temp_value=[]
            for (let item in inValue)
                temp_value.push(getFromReducer(reducer,String(inValue[item])))
            answer=temp_value
        }
        else if(exampleValue==='array_int'){
            let temp_value=[]
            for (let item in inValue)
                temp_value.push( parseInt(getFromReducer(reducer,String(inValue[item]))) )
            answer=temp_value
        }
        else if(exampleValue==='another')
            answer=inValue
        if(mode==='strict')
            return answer
        else{
            if( !!answer && typeof(answer)!==typeof([]) || typeof(answer)===typeof([]) && answer.length>0)
                return  answer
            else if(!answer && (exampleValue==='int' || exampleValue==='select_int')){
                return  0
            }
            else if(!answer){
                return  ''
            }
            else
                return null
        }
    }

    const formTransform = (formIn,formExample,mode='agile',formConverter=null) => {  //обработчик изменения поля  mod: [strict, agile]
        let formOut = {}
        for (let key in formExample){
            if(formConverter===null){
                formOut[key] = getFromConfig(formIn[key],formExample[key],mode,REDUCER)
            }
            else
                formOut[key] = getFromConfig(formIn[formConverter[key]],formExample[key],mode,REDUCER)
        }
        return formOut
    }

    return {formTransform,getFromConfig}
}

/*
    >пример содержит названия полей(ключи) и какой тип должен быть у значения
    >Список типов и их сокращений:
    >>...тут должен быть список сокращений типов (select_string - выбрать из сокращений одно значение - на выходе строка)

    сценарии работы:
        mode - выполнять подгон нулевых значений (agile) или передавать напрямую (strict)
        1.можно передать входную форму и пример выходной формы - все совпадающие поля будут переведены и отформатированы
        2.можно передать входную форму, пример выходной формы, форму преобразования(ключи - выход, значения - вход)
        3.{...to be continued}
 */
