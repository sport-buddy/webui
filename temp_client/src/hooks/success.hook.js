import {useState, useCallback} from 'react'

export const useSuccess = () => {

    const SUCCESS_STATUS=200

    const clearClassName = (temp,className='') => {
        temp.className = className;
    }

    const successStatusNotification = async (successStatus) => {
        let temp = document.getElementsByClassName('content')
        temp=temp[0]
        clearClassName(temp,'content')
        if(successStatus===SUCCESS_STATUS){
            temp.className = temp.className + ' success'
        }
        else{
            temp.className = temp.className + ' failed'
        }
        setTimeout(clearClassName,4000,temp,'content');
    }

    const alertSuccessStatus = async (successStatus) => {
        if(successStatus===SUCCESS_STATUS){
            alert('success')
        }
        else{
            alert('something go bad')
        }
    }

    return {successStatusNotification,alertSuccessStatus}
}

/*
    >пример содержит названия полей(ключи) и какой тип должен быть у значения
    >Список типов и их сокращений:
    >>...тут должен быть список сокращений типов (select_string - выбрать из сокращений одно значение - на выходе строка)

    сценарии работы:
        mode - выполнять подгон нулевых значений (agile) или передавать напрямую (strict)
        1.можно передать входную форму и пример выходной формы - все совпадающие поля будут переведены и отформатированы
        2.можно передать входную форму, пример выходной формы, форму преобразования(ключи - выход, значения - вход)
        3.{...to be continued}
 */
