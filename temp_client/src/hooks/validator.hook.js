import {useState, useCallback, useEffect} from 'react'
import {useHttp} from '../hooks/http.hook'

export const useValidator = () => {
    const {request} = useHttp()
    const [token, setToken] = useState("")
    const [refreshToken, setRefToken] = useState("")
    const [valid, setValid] = useState(false)

    const validator = useCallback((rule,error) => {
        if (!rule()){
            setValid(false)
            error() // отображать
        }
        else{
            setValid(true)
            //не отображать
        }
    }, [])


    const logout = useCallback(() => {
        setUserId(null)
        setToken(null)
    }, [])

    return { validator, valid}
}

