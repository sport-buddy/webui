import React,{useState} from 'react'
import {Route, BrowserRouter} from 'react-router-dom'
import {useRoutes} from './routes.js'
import {useAuth} from './hooks/auth.hook'
import {AuthContext} from './context/AuthContext'
import './css/App.css'
import './css/style.css'

function App() {
    const { login, logout, refreshingAccess, userId, token, refreshToken, ready} = useAuth()
    const isAuthenticated = !!token
    const routes = useRoutes(isAuthenticated) //Authorization chek instead "true"
    if (!ready) {
        return <p>Т УТ должен быть loader </p>
    }
    return (
        <AuthContext.Provider value={{
            token, refreshToken, userId, login, logout, refreshingAccess
        }}>
            <BrowserRouter>
                <div className="container">
                    {routes}
                </div>
            </BrowserRouter>
        </AuthContext.Provider>
    )
}

export default App