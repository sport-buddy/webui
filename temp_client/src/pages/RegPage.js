import React, {useContext, useEffect, useState} from 'react'
import {Link} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {InputField} from "../components/MainElements";
import '../css/AuthAndRegPage.css'

const SUCCESS_STATUS=200

export function RegPage() {
  const { loading, request, error, clearError } = useHttp()
  const [valid, setValid] = useState(false)
  const auth= useContext(AuthContext)

  const [form, setForm] = useState({
    username: '', password: '', password_check: ''
  })

  const changeHandler = event => {  //я не понимаю почему это работает, это тотальный кромешный ад
    setForm({ ...form, [event.target.name]: event.target.value })
    form[event.target.name]= event.target.value
  }

  const rule = (pass1,pass2) =>{
    let ruleAns = pass1 === pass2
    ruleAns = ruleAns && pass1.length>7;
    ruleAns = ruleAns && pass2.length>7;
    return ruleAns
  }

  const validator = form =>{
    const message = document.getElementById('error_message_valid')
    if (!rule(form.password,form.password_check)){
      setValid(false)
      message.className = 'shown-block' // отображать
    }
    else{
      setValid(true)
      message.className="hidden-block" //не отображать
    }
  }
  const spec_changeHandler = event => {
    changeHandler(event)
    validator(form)
  }

  const registerHandler = async () => {
    try {
      const message = document.getElementById('error_message_response')
      if (valid){
        const data = await request('/auth/signup', 'POST', {...form})
        console.log('data from request: ', data)
        if(data.status !== SUCCESS_STATUS){
          message.className="shown-block" // отображать
        }
        else{
          console.log('[user id , access token] from data:', data["id_user"], data["access_token"])
          auth.login(data["id_user"], data["access_token"])
        }
      } 
    } catch (e) {}
  }

  return (
    <div className="background-wrapper">

        <div className="title-wrap">
          <h1 className="title">Sport Buddy</h1>
        </div>

        <div className="wrap1">
          <div className="wrap2">
            <div>

              <InputField
                  id='username'
                  outText="Введите свой никнейм"
                  name="username"
                  valueName={form.username}
                  changeHandler={changeHandler}
              />

              <InputField
                  id='password'
                  outText="Введите пароль"
                  typeName="password"
                  name="password"
                  valueName={form.password}
                  changeHandler={spec_changeHandler}
              />

              <InputField
                  id='password_twi'
                  outText="Подтвердите пароль"
                  typeName="password"
                  name="password_check"
                  valueName={form.password_check}
                  changeHandler={spec_changeHandler}
              />

            </div>
          </div>

          <div className="card-action">

            <div id="error_message_valid" className="hidden-block">
              <span
                  className="warning"
              >
                Пароли не совпадают или не соответствуют требованиям!
              </span>
            </div>

            <div id="error_message_response" className="hidden-block">
              <span
                  className="warning"
              >
                Такой пользователь уже существует!
              </span>
            </div>


            <button
                id = "btn"
                className="btn"
                disabled={!valid}
                onClick={registerHandler}
            >
              Зарегистрироваться
            </button>

            <Link to="/login">
              Вернуться к входу
            </Link>

          </div>

        </div>

    </div>
  )
}
