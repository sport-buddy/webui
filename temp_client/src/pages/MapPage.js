import React, {useContext, useCallback, useEffect, useState} from 'react'
import {Navbar} from '../components/NavBar'
import {YMaps, Map} from 'react-yandex-maps';

export const MapPage = () => {

    return (
        <>
            <YMaps>
                <div className="content-wrapper">
                    <Navbar />
                    <div className="content">

                        <div id="map">
                            <Map
                                defaultState={{
                                    center: [59.9715, 30.321],
                                    zoom: 18,
                                    controls: ['zoomControl', 'fullscreenControl'],
                                }}
                                modules={['control.ZoomControl', 'control.FullscreenControl']}
                                width={"100%"}
                                height={"100%"}
                            />
                        </div>

                    </div>
                </div>
            </YMaps>
        </>

    )
}
