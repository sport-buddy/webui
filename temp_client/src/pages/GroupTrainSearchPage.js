import React, {useContext, useCallback, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useTransform} from '../hooks/transform.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {SearchResult} from '../components/SearchResult'
import {InputField, Button, PopUpDiv, MultiSelectField} from '../components/MainElements'
import '../css/TSP.css'
import {UserProfile} from "../components/UserProfile";
import {useSuccess} from "../hooks/success.hook";

export const GroupTrainSearchPage = () => {
    const {alertSuccessStatus} = useSuccess()
    const { loading, request, error, clearError } = useHttp()
    const {formTransform,getFromConfig} = useTransform()
    const auth= useContext(AuthContext)

    const formAPIExample = {
        id_level: 'array_int',
        location: 'array_string',
        person_sports: 'array_string'
    }

    const [form, setForm] = useState({
        id_level: [],
        location: [],
        person_sports: [],
    })

    const formJoinAPIExample = {
        created_at: 'string',
        id_from: 'int',
        id_to: 'int',
        id_training: 'int',
        seen: 'bool',
        status: 'string',
        type: 'string'
    }

    const formConvertToJoinAPI ={  //left(key) - formIn, right (value) - formOut
        //created_at: new Date,
        //id_from: auth.userId,
        id_to: 'owner',
        id_training: 'id_training',
        //seen: 'another',
        //status: 'another',
        type: 'kind'
    }

    var formJoinAPI = {}

    const [formAPI, setFormAPI] = useState({})

    const itemExample={
        comment: 'string',
        fee: 'string',
        id_level: 'select_string',
        location: 'select_string',
        meet_date: 'UI_date',
        owner: 'string', //владелец, админ тренировки
        participants_ids: 'another',
        sport: 'select_string',
        training_duration: 'Bk_time'
    }

    const ITEM_REDUCE={
        "owner": "Организатор тренировки",
        "comment": "Коментарий",
        "fee": "Стоимость участия",
        "id_level": "Уровень подготовки",
        "location": "Локация",
        "meet_date": "Дата проведения",
        "participants_ids": "id участников",
        "sport": "Спорт",
        "training_duration": "Длительность тренировки"
    }

    const PREITEM_REDUCE={
        "id_level": "Уровень подготовки",
        "location": "Локация",
        "sport": "Вид спорта",
        "meet_date": "Дата проведения"
    }

    const [items,setItems] = useState([])

    const parseItem = (item,reduce,itemClass="") => {  // расшифровать из сокращений ITEM_REDUCE ответы
        const res=[]
        for (let key in reduce) {
            if(item.hasOwnProperty(key) && typeof(reduce[key])=='string' && item[key]!==''){
                let parseRes
                let temp_res
                if (key==='owner'){
                    parseRes = <UserProfile id={item[key]}/>
                }
                else if(key==='participants_ids'){
                    parseRes=[]
                    for(let i of item[key])
                        parseRes.push(<><UserProfile id={i}/> </>)
                }
                else{
                    parseRes=getFromConfig(item[key],itemExample[key])
                }
                temp_res=<div className={itemClass} key={key}> {reduce[key]}: {parseRes}</div>
                res.push(temp_res)
            }
        }
        return res
    }

    const joinHandler= async (id) => {  // обработчик кнопки присоединиться
        console.log('>>join handler id:',id)
        console.log('>>all JoinAPIForm:',formJoinAPI)
        console.log('>>API to request:',formJoinAPI[id])
        try {
            const data =await request(`/messenger/request`, 'POST', {...formJoinAPI[id]}, {   //метод POST но подразумевается GET
                'X-Auth-Token': `${auth.token}`
            })
            await console.log('успех')
            await alertSuccessStatus(data.status)
        } catch(e){
            console.log('успех')
        }
        document.location.reload(); //наши любимые костыли
    }

    const arrayTransformer= (parseTargetArray,formExample) => {  // трансформер массива
        let answer = {}
        for (let item of parseTargetArray){
            answer[item.id_training] = (formTransform(item,formExample,'agile',formConvertToJoinAPI))
            answer[item.id_training]['id_from'] = auth.userId
            answer[item.id_training]['created_at'] = (new Date).toISOString()
            answer[item.id_training]['seen'] = false
            answer[item.id_training]['status'] = 'accepted'
        }
        console.log('response parse:',answer)
        return answer
    }

    const responseParse = async (dataArr) => {  // пробежаться по массиву ответа и сформировать найденные тренировки
        formJoinAPI = arrayTransformer(dataArr,formJoinAPIExample,'agile')
        for (let item of dataArr) {
            let parsedItem = parseItem(item,ITEM_REDUCE)
            let parsedPreItem = parseItem(item,PREITEM_REDUCE, "pre-item")
            console.log('parsedItem:',parsedItem)
            let DOMItem =
                <div className="item-wrap" key={item.id_training}>
                        <PopUpDiv
                            id={item.id_training+'popUp'}
                            content={
                                <div className="item-wrapper">
                                    {parsedPreItem}
                                </div>
                            }
                        >
                            {parsedItem}
                            <div className="join-btn" onClick={() => joinHandler(item.id_training)}>
                                +
                            </div>
                        </PopUpDiv>

                </div>
            setItems([...items , DOMItem])
            items.push(DOMItem)
        }
    }

    const changeHandler = event => {  //обработчик изменения поля
        changeValueHandler(event.target)
    }

    const changeValueHandler = target => {
        setForm({ ...form, [target.name]: target.value })
        form[target.name]= target.value
        setFormAPI(formTransform(form,formAPIExample))
    }

    const searchBtnHandler= async () => {  // обработчик кнопки "найти"
        setItems([])
        items.length = 0 //очистить массив ответов
        console.log('Data to request response:',formAPI)
        try {
            const data =await request(`/training/search`, 'POST', {...formAPI}, {   //метод POST но подразумевается GET
                'X-Auth-Token': `${auth.token}`
            })
            console.log('Data from training response:',data)
            await responseParse(data)
        } catch(e){}
    }

    return (
        <>
            <div className="content-wrapper">
                <Navbar />
                <div className="content" id="tsp">

                    <button
                        id="save-change-btn"
                        className="btn"
                        disabled={loading}
                        onClick={searchBtnHandler}
                    >
                        Найти
                    </button>

                    <div className="tsp-first-include-wrapper">
                        <div className='tsp-item'>
                            <MultiSelectField
                                id="person_sports"
                                innerText="Виды спорта"
                                isDisabled={loading}
                                name="person_sports"
                                value={form.person_sports}
                                onChange={changeValueHandler}
                                options={[
                                    'футбол',
                                    'атлетика',
                                    'баскетбол',
                                    'бег'
                                ]}
                            />
                        </div>

                        <div className='tsp-item'>
                            <MultiSelectField
                                innerText="Подготовка"
                                isDisabled={loading}
                                name="id_level"
                                value={form.id_level}
                                onChange={changeValueHandler}
                                options={[
                                    'Не выбран',
                                    'Начинающий',
                                    'Продвинутый',
                                    'Мастер',
                                ]}
                            />
                        </div>

                        <div className='tsp-item'>
                            <MultiSelectField
                                id="location"
                                isDisabled={loading}
                                name="new_location"
                                value={form.location}
                                innerText="Локация"
                                onChange={changeValueHandler}
                                options={[
                                    'Автово',
                                    'Невский Проспект',
                                    'Пионерская',
                                    'Горьковская'
                                ]}
                            />
                        </div>

                    </div>

                    <SearchResult items={items}/>

                    <Button
                        typeName='link'
                        id="change-btn"
                        className="btn"
                        disabled={loading}
                        href='/createGroup'
                        innerText="Создать тренировку"
                    />

                </div>
            </div>
        </>

    )
}
