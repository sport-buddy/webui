import React,  {useContext, useCallback, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useTransform} from '../hooks/transform.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {Image, InputField, MultiSelectField, SelectField} from '../components/MainElements'
import '../css/Profile.css'
import {useSuccess} from "../hooks/success.hook";


export const ProfilePage = () => {
    const {successStatusNotification} = useSuccess()
    const { loading, request } = useHttp()
    const [guestMod, setGuestMod] = useState(false)
    const {formTransform} = useTransform()
    const auth= useContext(AuthContext)

    const formUIExample = {
         about :  'string',
         date_of_birth :  'string',
         email :  'string',
         height : 'string',
         id_level : 'select_string',
         id_user : 'another',
         location :  'array_string',
         name :  'string',
         second_name :  'string',
         sex :  'select_string',
         person_sports : 'array_string',
         weight : 'string'
    }

    const formAPIExample = {
        about :  'string',
        date_of_birth : 'string',
        email :  'string',
        height : 'int',
        id_level : 'select_int',
        id_user : 'another',
        location :  'array_string',
        name :  'string',
        second_name :  'string',
        sex :  'select_string',
        person_sports : 'array_string',
        weight : 'int'
    }

    const [formUI, setFormUI] = useState({})

    let formAPI = {}

    const schema = { //для валидации
        name: ['required', 'not_empty'],
        second_name: ['required', 'not_empty']
    };

    const changeBtnHandler = event => {  // обработчик кнопки "изменить данные профиля"try
        //setChanging(!isChanging)
    }

    const ruleChek = (regExp, value) => {
        return regExp.test(value);
    }

    const validator = (formValueName,value) => {
        if (formValueName==='name' || formValueName==='second_name') {
            return !ruleChek(/[^a-zA-Zа-яА-Я']+/, value); //проверяем наличие неразрешенных символов
        }
        if (formValueName==='about') {
            return !ruleChek(/[^a-zA-Zа-яА-Я\d,._\-\s()']+/, value);
        }
        if (formValueName==='email') {
            let res = !ruleChek(/[^a-zA-Z\d,._\-\s()'@]+/, value);
            res = res && value[0]!=='.' && value[value.length]!=='.'
            return res
        }
        return true
    }

    const changeHandler = event => {  //обработчик изменения поля
        changeValueHandler(event.target)
    }

    const changeValueHandler = target => {
        if(validator(target.name, target.value)){
            setFormUI({ ...formUI, [target.name]: target.value })
            formUI[target.name]= target.value
            formAPI = formTransform(formUI,formAPIExample) //это не оптимально, хочу переделать чтобы было не так
        }
        else{
            target.className = target.className + ' invalid-input'
        }
    }

    const saveBtnHandler= async () => {  // обработчик кнопки "сохранить изменения"
        formAPI = formTransform(formUI,formAPIExample)
        console.log('formUI: ',formUI)
        try {
            const data = await request(`/user/profile`, 'PUT', {...formAPI}, {
                'X-Auth-Token': `${auth.token}`
            })
            await successStatusNotification(data.status)
        } catch(e){
            console.log('Ошибка при запросе',e)
        }
    }

    const getIdFromPathname = () => {
        let tempId = window.location.pathname.replace(/\S*\//,'')
        if(tempId==='profile')
            return auth.userId
        else
        {
            setGuestMod(true)
            return  parseInt(tempId)
        }
    }

    const fetchProfileData = useCallback(async () => {

        try {
            const fetched = await request(`/user/profile/${getIdFromPathname()}`, 'GET', null, {
                'X-Auth-Token': `${auth.token}`
            })
            console.log('GET data: ',fetched)
            await successStatusNotification(fetched.status)
            await setFormUI(formTransform(fetched,formUIExample,'strict'))
            console.log('UI GET data: ',formUI)

        } catch (e) {}
    }, [auth.userId, request])

    useEffect(() => {
        fetchProfileData()

    }, [fetchProfileData])

    return (
        <>
            <Navbar />
            <div className="content-wrapper">
                <div className="content">

                <div className="first-include-wrapper">

                    <div className="first-wrapper">

                        <div className="first-include-wrapper">

                            <div className="FIO-wrapper">

                                <InputField
                                    id='firstname'
                                    inputType="validInput"
                                    outText="Имя"
                                    isDisabled={loading}
                                    name="name"
                                    data={formUI}
                                    valueName={formUI.name}
                                    changeHandler={changeHandler}
                                />

                                <InputField
                                    id='second_name'
                                    outText="Фамилия"
                                    isDisabled={loading}
                                    name="second_name"
                                    valueName={formUI.second_name}
                                    changeHandler={changeHandler}
                                />

                            </div>

                        </div>


                        <InputField
                            id='about'
                            outText="Информация о себе"
                            isDisabled={loading}
                            name="about"
                            valueName={formUI.about}
                            inputType="textArea"
                            changeHandler={changeHandler}
                            className='about-wrapper'
                        />

                        <InputField
                            id='email'
                            outText="E-mail"
                            isDisabled={loading}
                            name="email"
                            valueName={formUI.email}
                            changeHandler={changeHandler}
                        />


                    </div>

                    <div className="second-wrapper">

                        <InputField
                            typeName="date"
                            id='DOB'
                            outText="Дата рождения"
                            isDisabled={loading}
                            name="date_of_birth"
                            valueName={formUI.date_of_birth}
                            changeHandler={changeHandler}
                        />

                        <div className="profile-selectors-wrapper">

                            <div className="second-include-wrapper">

                                <div className="profile-selectors-item">
                                    <SelectField
                                        outText="Пол"
                                        isDisabled={loading}
                                        name="sex"
                                        value={formUI.sex}
                                        onChange={changeValueHandler}
                                        options={[
                                            'муж',
                                            "жен",
                                            "не выбрано"
                                        ]}
                                    />
                                </div>

                                <div className="profile-selectors-item">
                                    <SelectField
                                        outText="Подготовка"
                                        isDisabled={loading}
                                        name="id_level"
                                        value={formUI.id_level}
                                        onChange={changeValueHandler}
                                        options={[
                                            'Не выбран',
                                            'Начинающий',
                                            'Продвинутый',
                                            'Мастер',
                                        ]}
                                    />
                                </div>
                                <div className="profile-selectors-item">
                                    <MultiSelectField
                                        id="location"
                                        outText="Локация"
                                        isDisabled={loading}
                                        name="location"
                                        value={formUI.location}
                                        onChange={changeValueHandler}
                                        options={[
                                            'Автово',
                                            'Невский Проспект',
                                            'Пионерская',
                                            'Горьковская'
                                        ]}
                                    />
                                </div>
                            </div>

                            <div className="second-include-wrapper">

                                <div className="profile-selectors-item">
                                    <InputField
                                        id="height"
                                        outText="Рост"
                                        isDisabled={loading}
                                        name="height"
                                        mask="999"
                                        maskPlaceholder=" "
                                        valueName={formUI.height}
                                        changeHandler={changeHandler}
                                    />
                                </div>

                                <div className="profile-selectors-item">
                                    <InputField
                                        id="weight"
                                        outText="Вес"
                                        isDisabled={loading}
                                        name="weight"
                                        mask="999"
                                        maskPlaceholder=" "
                                        valueName={formUI.weight}
                                        changeHandler={changeHandler}
                                    />
                                </div>

                                <div className="profile-selectors-item">
                                    <MultiSelectField
                                        id="person_sports"
                                        outText="Виды спорта"
                                        isDisabled={loading}
                                        name="person_sports"
                                        value={formUI.person_sports}
                                        onChange={changeValueHandler}
                                        options={[
                                            'футбол',
                                            'атлетика',
                                            'баскетбол',
                                            'бег'
                                        ]}
                                    />
                                </div>

                            </div>

                        </div>
                            <>
                                {
                                    !guestMod &&
                                    <div className="example-wrapper">
                                        <button
                                            id="change-btn"
                                            className="btn"
                                            disabled={loading}
                                            onClick={saveBtnHandler}
                                        >
                                            Сохранить изменения
                                        </button>
                                    </div>
                                }
                            </>

                    </div>

                </div>

            </div>
            </div>
        </>

  )
}
