import React, {useContext, useCallback, useEffect, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {UserProfile} from "../components/UserProfile"
import {InputField, PopUpDiv, Button} from '../components/MainElements'
import '../css/Messenger.css'
import '../css/TSP.css'
import {SearchResult} from "../components/SearchResult"

const DialogList = ({dialogs,onClick}) => { //name, content[] - массив от 0 до 6, itemClass - класс общий для всех элементов, rawClass
    const auth= useContext(AuthContext)

    if (!dialogs.length) {
        return <p className="center">Сообщений пока нет</p>
    }

    return (
        <>
            <div className="item-wrap">
                { dialogs.map((dialog, index) => {
                    let id
                    if (dialog.id_from===auth.userId) //если сообщение от нас
                        id = dialog.id_to
                    else
                        id = dialog.id_from         //если сообщение от собеседника
                    return (
                        <div className="item-wrapper" key={index} onClick={onClick} id={id}>
                            <UserProfile id={id}/>
                        </div>
                    )
                }) }
            </div>
        </>
    )

}

const formParser = (form) => {
    let ans='{'
    for (let key in form){
        if(typeof(form[key])!=='number')
            ans=ans+'\"'+String(key)+'\"'+':'+'\"'+String(form[key])+'\"'+','
        else
            ans=ans+'\"'+String(key)+'\"'+':'+String(form[key])+','
    }
    ans = ans.slice(0, ans.length-1)
    return ans+'}'
}

const responseParser = (form) => {

}

const Dialog= ({dialog_id,onClose,socket}) => { //dialog_id - id пользователя с которым ведется диалог


    const {request, loading} = useHttp()
    const auth= useContext(AuthContext)
    const [messageToSend, setMessageToSend] = useState('')
    const [newMessages, setNewMessages] = useState([])

    const now = new Date()
    const formRequest = {
        created_after: now.toISOString(),
        id_users: [parseInt(dialog_id),auth.userId]
    }

    const changeHandler = event => {  //обработчик изменения поля
        setMessageToSend(event.target.value)
        console.log(newMessages)
    }

    const sendHandler = event => {  //обработчик изменения поля
        let in_cur_moment = new Date()
        let formMessageToSend = {
            content: messageToSend,
            created_after: in_cur_moment.toISOString(),
            id_from: auth.userId,
            id_mes : 0,
            id_to: parseInt(dialog_id),
        }
        let parsedForm = formParser(formMessageToSend)
        setMessageToSend('')
        setNewMessages([...newMessages, formMessageToSend])
        try {
            socket.send(parsedForm);
        } catch (e) {}
    }

    const fetchMessageList = useCallback(async () => {
        try {
            console.log('>>fetched:',formRequest)
            const fetched = await request(`/messenger/dialogs/search`, 'POST', {...formRequest}, {
                'X-Auth-Token': `${auth.token}`
            })
            setNewMessages(fetched)
            console.log('>>fetched:',fetched)
        } catch (e) {}
    }, [])

    useEffect(() => {
        fetchMessageList()
    }, [fetchMessageList])

    const onMessage = (event) => {
        console.log(`[message] Данные получены с сервера: ${event.data}`,'тип данных:',typeof(event.data));

        let data = JSON.parse (event.data);
        console.log(`[json parse] Данные получены с сервера: ${data}`);
        console.log(`[newMessages] Данные : ${newMessages}`);
        setNewMessages([...newMessages,data])
    };

    const onOpen = (event) => {
        console.log("[open] Соединение установлено");
    };

    useEffect(() => {
        console.log('>>>socket status on open:', socket)
        socket.onopen = onOpen
        socket.onmessage = onMessage
    }, [])

    return (
        <>
            <div className="mini-btn" onClick={onClose}>
                Назад к диалогам
            </div>
            <div className="dialog-wrapper">
                <div className="dialog-container">
                    <UserProfile id={dialog_id}/>
                    <div className="messages-container">
                        { newMessages.map((message, index) => {
                            if (message.id_from!==auth.userId) //если сообщение от нас
                                return (
                                    <div className="message-to" key={index}>
                                        Тут сообщения нам {message.content}
                                    </div>
                                )
                            else                 //если сообщение от собеседника
                                return (
                                    <div className="message-from" key={index}>
                                        Тут сообщения от нас {message.content}
                                    </div>
                                )
                        }) }

                    </div>
                    <div className="new-message-field">
                        <InputField
                            id='text-input'
                            innerText="Сообщение"
                            isDisabled={loading}
                            name="name"
                            valueName={messageToSend}
                            changeHandler={changeHandler}
                        />
                        <Button innerText="Отправить" onClick={sendHandler}/>
                    </div>
                </div>
            </div>
        </>
    )

}


export const MessengerPage = () => {
    const { loading, request, error, clearError } = useHttp()
    const auth= useContext(AuthContext)
    //const [redirectLink, setRedirectLink] = useState('')
    const [dialogs, setDialogs] = useState([])
    const [isChanging, setChanging] = useState(false)

    const fetchDialogList = useCallback(async () => {
        try {
            const fetched = await request(`/messenger/dialogs`, 'GET', null, {
                'X-Auth-Token': `${auth.token}`
            })
            setDialogs(fetched)
        } catch (e) {}
    }, [auth.userId, request])

    useEffect(() => {
        fetchDialogList()
    }, [fetchDialogList])

    const [curDialog,setCurDialog] = useState(0)

    const openDialogHandler= (event) => {  // обработчик кнопки присоединиться
        console.log('event.target.id:',event.target.id)
        setCurDialog(event.target.id)
    }

    const closeDialogHandler= (event) => {  // обработчик кнопки присоединиться
        setCurDialog(0)
    }

    var socket= new WebSocket(`ws://localhost:3000/messenger?X-Auth-Token=${auth.token}`)

    return (
        <>
            <div className="content-wrapper">
                <Navbar />
                <div className="content">
                    {!curDialog  && <DialogList dialogs={dialogs} onClick={openDialogHandler}/>}
                    {!!curDialog  && <Dialog dialog_id={curDialog} onClose={closeDialogHandler} socket={socket}/>}
                </div>
            </div>
        </>
    )
}
