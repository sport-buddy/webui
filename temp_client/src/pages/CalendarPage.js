import React, {useContext, useCallback, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useTransform} from '../hooks/transform.hook'
import {useSuccess} from '../hooks/success.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {Calendar} from '../components/Calendar'
import {InputField, Button, MultiSelectField, SelectField, PopUpDiv} from '../components/MainElements'
import '../css/Calendar.css'
import {NavLink} from "react-router-dom";

const SUCCESS_STATUS = 200

export const CalendarPage = () => {
    const currDate = new Date()
    const weekDaysFull = {
        1: "Понедельник",
        2: "Вторник",
        3: "Среда",
        4: "Четверг",
        5: "Пятница",
        6: "Суббота",
        0: "Воскресенье",
    }
    const currWeekDay = useState(weekDaysFull[currDate.getDay()])
    const {successStatusNotification} = useSuccess()
    const {loading, request, error, clearError} = useHttp()
    const {formTransform} = useTransform()
    const [selectedDate, newSelectedDate] = useState(new Date())
    const [currMonth, setCurrMonth] = useState(currDate.getMonth())
    const auth = useContext(AuthContext)


    const formAPIExample = {
        comment: 'string',
        fee: 'int',
        id_level: 'select_int',
        id_training: 'int',
        kind: 'string',
        location: 'select_string',
        meet_date: 'date',
        owner: 'another',
        participants_ids: 'another',
        sport: 'select_string',
        training_duration: 'time'
    }

    const [formAPI, setFormAPI] = useState({})

    const [formParsedTrainings, setFormParsedTrainings] = useState([{}])
    //var formParsedTrainings = []


    const formParsedTrainingsItemExample = {
        comment: 'string',
        fee: 'string',
        id_level: 'select_string',
        id_training: 'int',
        kind: 'string',
        location: 'select_string',
        meet_date: 'string',
        owner: 'another',
        participants_ids: 'another',
        sport: 'select_string',
        training_duration: 'string'
    }

    const arrayTransformer = (parseTargetArray, formExample) => {  // обработчик кнопки присоединиться
        let answer = []
        for (let item of parseTargetArray) {
            if (typeof (item) !== 'Int')
                answer.push(formTransform(item, formExample, 'agile'))
        }
        return answer
    }

    const changeMonth = (direction) => {  // обработчик кнопки присоединиться
        if (direction === "+") {
            const newDate = selectedDate
            newDate.setMonth(newDate.getMonth() + 1)
            newSelectedDate(newDate)
            setCurrMonth(newDate.getMonth())
        } else {
            const newDate = selectedDate
            newDate.setMonth(newDate.getMonth() - 1)
            newSelectedDate(newDate)
            setCurrMonth(newDate.getMonth())
        }
        console.log(selectedDate)
    }

    const getMonthRus = (month) => {  // обработчик кнопки присоединиться
        const months = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"]
        return months[month]
    }

    const fetchCalendarData = useCallback(async () => {

        try {
            const fetchedArray = await request(`/user/${auth.userId}/training`, 'GET', null, {
                'X-Auth-Token': `${auth.token}`
            })
            await successStatusNotification(fetchedArray.status)
            setFormParsedTrainings(arrayTransformer(fetchedArray, formParsedTrainingsItemExample))
        } catch (e) {
        }
    }, [auth.userId, request])

    useEffect(() => {
        fetchCalendarData()
    }, [fetchCalendarData])


    return (
        <>
            <Navbar/>
            <div className="content-wrapper">
                <div className="content" id="tsp">
                    <main className="calendar-contain">
                        <section className="title-bar">
                            <span
                                className="title-bar__year">Тренировки > {getMonthRus(selectedDate.getMonth())} {selectedDate.getFullYear()}</span>
                        </section>

                        <div className="sidebar__nav">
                            <NavLink className="sidebar__nav-item" to='/createGroup'><img
                                className="icon icons8-Plus-Math" width="22px"
                                height="22px"
                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAWUlEQVRYR+2WMQoAIAgA9f+PrsWmEMQSEa7Z8rzEUmle2pxfABhvYFkPpQtJb7TEAGAAAxgoM3AO/v1YXoPPm4TtANHKy64AAAxgAANjDERB3bjXXzEA8w1s3k4WIU0YaEoAAAAASUVORK5CYII="/></NavLink>
                            <span className="sidebar__nav-item" onClick={() => changeMonth("-")}><img
                                className="icon icons8-Up" width="22px"
                                height="22px"
                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxklEQVRYR+3VwQ2DMAyF4Z8NOkI36AodpZ2sjMAoHaWdoJWlIEXI4RHnwMWROAH2ZxuSiZPXdHJ+EjDagUcZ4Rwd5QjAkr9K4icQQkQBdfK1+BAiAvCShxG9gG3b6xGExtED8Gb+K6VbnNA3cRTQCl4DzNKNOALYC7oFdCMUQFXkAboQCvAGbkDrF2sBasQXuAIfb7NSgAtwB5bGTrcHWBFWhF3uUgC1wyqAen/4NExAdiA7kB3IDmQH5GGjHhg9DVV8eT8Bf2HqNiEP+isaAAAAAElFTkSuQmCC"/></span>
                            <span className="sidebar__nav-item" onClick={() => changeMonth("+")}><img
                                className="icon icons8-Down" width="22px"
                                height="22px"
                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAA4UlEQVRYR+2WgQnCMBBFXzdwBSdwBUfRyXQER3EUN1AOEgnxLrkkhYCkUFpI7/+Xf1fajcnHNtmfBTCawDu0sFunuzAYL4CVwEpgJbAS+P8EDsAZeBj/DbUELsAznKpE7WMkxSfgCtwVhRKAmN+AF3AM1x+JGkAUkUINwgKo1X1BagDyYElMA3Cbi7gHoASRAzSZtwBYEClAs3krgAYhQxbnI73XBrbrLdCK0p3m69bbYv79e2cgF9Agms17WpCCdPU830lvAlFHIORw93xvALO33oXRBLw+uw/hsHEUmJ7ABzErNiGyzfJcAAAAAElFTkSuQmCC"/></span>
                        </div>
                        <Calendar token={auth.token} formTraining={formParsedTrainings}
                                  curMonth={currMonth} currDate={currDate} currWeekDay={currWeekDay}/>
                    </main>
                </div>
            </div>
        </>
    )
}
