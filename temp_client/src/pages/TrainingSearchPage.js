import React, {useContext, useCallback, useEffect, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {useTransform} from '../hooks/transform.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {SearchResult} from '../components/SearchResult'
import {InputField, SelectField, Button, MultiSelectField, PopUpDiv} from '../components/MainElements'
import '../css/TSP.css'
import ReactInputMask from "react-input-mask";

export const TrainingSearchPage = () => {
    const { loading, request, error, clearError } = useHttp()
    const {formTransform, getFromConfig} = useTransform()
    const auth= useContext(AuthContext)

    const [form, setForm] = useState({
        age : '',
        id_level : [],
        location :  [],
        sex: '',
        sport: '',
        weight : ''
    })

    const [formAPIExample, setFormAPIExample] = useState({
        age_from: 'int',
        age_to: 'int',
        id_level: 'array_int',
        location: 'array_string',
        sex: 'select_string',
        sport: 'array_string',
        weight_from: 'int',
        weight_to: 'int'
    })

    const [formAPI, setFormAPI] = useState({
        age_from: 0,
        age_to: 1,
        id_level: [],
        location: [],
        sex: '',
        sport: [],
        weight_from: 0,
        weight_to: 0
    })

    const formJoinAPIExample = {
        created_at: 'string',
        id_from: 'int',
        id_to: 'int',
        id_training: 'int',
        seen: 'bool',
        status: 'string',
        type: 'string'
    }

    var formJoinAPI = {}

    const formConvertToJoinAPI ={  //left(key) - formIn, right (value) - formOut
        //created_at: new Date,
        //id_from: auth.userId,
        id_to: 'id_user',
        //id_training: 'id_user',
        //seen: 'another',
        //status: 'another',
        type: 'kind'
    }

    const itemExample={
        about: 'string',
        age: 'string',
        date_of_birth: 'date',
        email: 'string',
        height: 'string',
        id_level: 'select_string',
        location: 'select_string',
        name: 'string',
        person_sports: 'select_string',
        second_name: 'string',
        sex: 'select_string',
        weight: 'string'
    }

    const ITEM_REDUCE={
        "about": "О себе",
        "age": "Возраст",
        "date_of_birth": "Дата рождения",
        "email": "Email",
        "height": "Рост",
        "id_level": "Уровень подготовки",
        "location": "Локация",
        "name": "Имя",
        "person_sports": "Спорт",
        "second_name": "Фамилия",
        "sex": "Пол",
        "weight": "Вес"
    }

    const PREITEM_REDUCE={
        "name": "Имя",
        "age": "Возраст",
        "location": "Локация",
        "id_level": "Уровень подготовки"
    }

    const [items,setItems] = useState([])

    const parseItem = (item,reduce,itemClass="") => {  // расшифровать из сокращений ITEM_REDUCE ответы
        const res=[]
        for (let key in item) {
            if(reduce.hasOwnProperty(key) && typeof(reduce[key])=='string' && item[key]!==''){
                let parseRes=getFromConfig(item[key],itemExample[key])
                let temp_res=<div className={itemClass} key={key}>{reduce[key]}: {parseRes}</div>
                res.push(temp_res)
            }
        }
        return res
    }

    const formUiToJoin = (form) => {

    }

    const arrayTransformer= (parseTargetArray,formExample) => {  // обработчик кнопки присоединиться
        let answer = {}
        for (let item of parseTargetArray){
            answer[item.id_user] = (formTransform(item,formExample,'agile',formConvertToJoinAPI))
            answer[item.id_user]['id_from'] = auth.userId
            answer[item.id_user]['created_at'] = (new Date).toISOString()
            answer[item.id_user]['seen'] = false
            answer[item.id_user]['type'] = 'personal'
        }
        return answer
    }

    const responseParse = async (dataArr) => {  // пробежаться по массиву ответа и сформировать найденные тренировки
        formJoinAPI = arrayTransformer(dataArr,formJoinAPIExample,'strict')
        console.log('res JoinAPIForm:',formJoinAPI)
        for (let item of dataArr) {
            let parsedItem = parseItem(item,ITEM_REDUCE)
            let parsedPreItem = parseItem(item,PREITEM_REDUCE, "pre-item")
            console.log('parsedItem:',parsedItem)
            let DOMItem =
                <div className="item-wrap" key={item.id_user}>
                    <PopUpDiv
                        id={item.id_user+'popUp'}
                        content={
                            <div className="item-wrapper">
                                {parsedPreItem}
                            </div>
                        }
                    >
                        {parsedItem}
                        <div className="join-btn" onClick={() => joinHandler(item.id_user)}>
                            +
                        </div>
                    </PopUpDiv>

                </div>
            setItems([...items , DOMItem])
            items.push(DOMItem)
        }
    }

    const changeHandler = event => {  //обработчик изменения поля
        console.log('event handler target name and  value :',event.target.name, event.target.value)
        changeValueHandler(event.target)
    }

    const changeValueHandler = target => {
        console.log('value handler target name and  value :',target.name, target.value)
        setForm({ ...form, [target.name]: target.value })
        form[target.name]= target.value
        setFormAPI(formTransform(form,formAPIExample))
    }

    const joinHandler= async (id) => {  // обработчик кнопки присоединиться
        console.log('join handler id:',id)
        console.log('all JoinAPIForm:',formJoinAPI)
        console.log('API to request:',formJoinAPI[id])
        try {
            const data =await request(`/messenger/request`, 'POST', {...formJoinAPI[id]}, {   //метод POST но подразумевается GET
                'X-Auth-Token': `${auth.token}`
            })
            console.log('response:',data)
        } catch(e){}
    }

    const searchBtnHandler= async () => {  // обработчик кнопки "найти"
        console.log(' formUI to PUT:',form)
        console.log(' formAPI to PUT:',formAPI)
        items.length = 0 //очистить массив ответов
        try {
            const data =await request(`/user/profile/search`, 'POST', {...formAPI}, {
                'X-Auth-Token': `${auth.token}`
            })
            console.log('Data from training response:',data)
            await responseParse(data)
        } catch(e){}
    }



    const avtoset = useCallback(async () => {
        setFormAPI(formTransform(form,formAPIExample))
    }, [form, formAPIExample])

    useEffect(() => {
        avtoset()
    }, [avtoset])

    return (
        <>
            <div className="content-wrapper">
                <Navbar />
                <div className="content" id="tsp">

                <button
                    id="save-change-btn"
                    className="btn"
                    disabled={loading}
                    onClick={searchBtnHandler}
                >
                    Найти
                </button>

                <div className="example-wrapper">
                    <button
                        id="save-change-btn"
                        className="btn"
                        disabled={loading}
                        onClick={changeHandler}
                    >
                        Найти партнера
                    </button>
                    <Button
                        typeName='link'
                        id="change-btn"
                        className="btn"
                        disabled={loading}
                        href='/searchGroup'
                        innerText="Найти группу"
                    />

                </div>

                <div className="second-include-wrapper">

                    <div className="first-include-wrapper">

                        <SelectField
                            innerText="Пол"
                            isDisabled={loading}
                            name="sex"
                            value={form.sex}
                            changeHandler={changeValueHandler}
                            options={[
                                'муж',
                                "жен",
                                "не выбрано"
                            ]}
                        />

                        <SelectField
                            innerText="Подготовка"
                            isDisabled={loading}
                            name="id_level"
                            value={form.id_level}
                            onChange={changeValueHandler}
                            options={[
                                'Не выбран',
                                'Начинающий',
                                'Продвинутый',
                                'Мастер',
                            ]}
                        />

                        <MultiSelectField
                            id="location"
                            isDisabled={loading}
                            name="new_location"
                            value={form.location}
                            innerText="Локация"
                            onChange={changeValueHandler}
                            options={[
                                'Автово',
                                'Невский Проспект',
                                'Пионерская',
                                'Горьковская'
                            ]}
                        />

                    </div>

                    <div className="first-include-wrapper">

                        <InputField
                            id="age"
                            innerText="Возраст"
                            isDisabled={loading}
                            name="age"
                            valueName={form.age}
                            mask="99-99"
                            maskPlaceholder="-"
                            changeHandler={changeHandler}
                        />

                        <InputField
                            id="weight"
                            innerText="Вес"
                            isDisabled={loading}
                            name="weight"
                            valueName={form.weight}
                            changeHandler={changeHandler}
                        />

                        <MultiSelectField
                            id="person_sports"
                            innerText="Виды спорта"
                            isDisabled={loading}
                            name="person_sports"
                            value={form.person_sports}
                            onChange={changeValueHandler}
                            options={[
                                'футбол',
                                'атлетика',
                                'баскетбол',
                                'бег'
                            ]}
                        />


                    </div>

                </div>
                <SearchResult items={items}/>
            </div>
            </div>
        </>

    )
}
