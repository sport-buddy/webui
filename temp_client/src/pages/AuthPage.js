import React, {useContext, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import '../css/AuthAndRegPage.css'
import {InputField} from "../components/MainElements";

const SUCCESS_STATUS=200

export function AuthPage() {
  const { loading, request, error, clearError } = useHttp()
  const auth= useContext(AuthContext)
  const [form, setForm] = useState({
    username: '', password: ''
  })

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const loginHandler = async () => {
    try {
      const data = await request('/auth/login', 'POST', {...form})
      if(data.status !== SUCCESS_STATUS){
        const message = document.getElementById('error_message')
        message.className="shown-block" // отображать
      }
      else {
        await auth.login(data["id_user"], data["access_token"])
      }
    } catch (e) {}
  }

  return (
      <div className="background-wrapper">
        <div className="wrap0">

          <div className="title-wrap">
            <h1 className="title">Sport Buddy</h1>
          </div>

          <div className="wrap1">
            <div>

              <div id="error_message" className="hidden-block">
                    <span className="warning">
                      Логин или пароль неверный
                    </span>
              </div>

              <div className="wrap1">

                <InputField
                    id='username'
                    outText="Введите свой никнейм"
                    name="username"
                    valueName={form.username}
                    changeHandler={changeHandler}
                />

                <InputField
                    id='password'
                    outText="Пароль"
                    name="password"
                    typeName="password"
                    valueName={form.password}
                    changeHandler={changeHandler}
                />

              </div>


            </div>
            <div className="card-action">
              <button
                  className="btn"
                  disabled={loading}
                  onClick={loginHandler}
              >
                Войти
              </button>
              <Link to="/sign-up">
                Хочу зарегистрироваться
              </Link>
            </div>

          </div>
        </div>
      </div>
  );

}
