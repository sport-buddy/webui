import React, {useContext, useCallback, useEffect, useState} from 'react'
import {Redirect, Link, NavLink} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {Image} from '../components/MainElements'
import '../css/menu.css'
import {SearchResult} from "../components/SearchResult";

export const MainMenuPage = () => {

    return (
        <>
            <div className="wrap0">
                <div className="wrap1">
                    <div id="menu" className="menu">
                        <li> <NavLink to="/calendar"> <Image src = {require('../img/cal.png')}/> Календарь</NavLink></li>
                        <li> <NavLink to="/search"> <Image src = {require('../img/pngegg.png')}/> Найти тренировку</NavLink></li>
                        <li> <NavLink to="/profile"> <Image src = {require('../img/prof.png')}/> Профиль</NavLink></li>
                        <li> <NavLink to="/map">  <Image src = {require('../img/map.png')}/> Карта</NavLink></li>
                    </div>

                </div>
            </div>
        </>

    )
}
