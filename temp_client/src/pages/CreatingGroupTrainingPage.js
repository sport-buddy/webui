import React, {useContext, useCallback, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useTransform} from '../hooks/transform.hook'
import {AuthContext} from '../context/AuthContext'
import {Navbar} from '../components/NavBar'
import {SearchResult} from '../components/SearchResult'
import {InputField, Button, MultiSelectField, SelectField} from '../components/MainElements'
import '../css/TSP.css'
import {useSuccess} from "../hooks/success.hook";

const SUCCESS_STATUS=200

export const CreatingGroupTraining = () => {
    const {successStatusNotification} = useSuccess()
    const { loading, request, error, clearError } = useHttp()
    const {formTransform} = useTransform()
    const [editMod, setEditMod] = useState(false)

    const auth= useContext(AuthContext)

    const formUIExample = {
        comment: 'string',
        fee:  'string',
        id_level:  'select_string',
        id_training: 'int',
        kind: 'string',
        location:  'select_string',
        meet_date:  'Spec_UI_date',
        owner: 'another',
        participants_ids: 'another',
        sport:  'select_string',
        training_duration:  'Spec_Bk_time'
    }

    const formAPIExample = {
        comment: 'string',
        fee:  'int',
        id_level:  'select_int',
        id_training: 'int',
        kind: 'string',
        location:  'select_string',
        meet_date:  'date',
        owner: 'another',
        participants_ids: 'another',
        sport:  'select_string',
        training_duration:  'time'
    }

    const [formAPI, setFormAPI] = useState({})

    const [form, setForm] = useState({
        comment: '',
        fee: '',
        id_level: '',
        id_training: 0,
        kind: 'group',
        location: '',
        meet_date: '',
        owner: auth.userId,
        participants_ids: [
            auth.userId
        ],
        sport: '',
        training_duration: ''
    })

    const changeHandler = event => {  //обработчик изменения поля
        changeValueHandler(event.target)
        //console.log(' formAPI after changes:',formAPI)
    }

    const changeValueHandler = target => {
        setForm({ ...form, [target.name]: target.value })
        form[target.name]= target.value
        setFormAPI(formTransform(form,formAPIExample))

    }

    const saveBtnHandler= async (editMod) => {  // обработчик кнопки "сохранить изменения"
        console.log(' formUI to PUT:',form)
        console.log(' formAPI to PUT:',formAPI)
        try {
            if(editMod){
                const data = await request(`/training/${getIdFromPathname()}`, 'PUT', {...formAPI}, {
                    'X-Auth-Token': `${auth.token}`
                })
                console.log(data)
                await successStatusNotification(data.status)
            }
            else{
                const data = await request(`/training`, 'POST', {...formAPI}, {
                    'X-Auth-Token': `${auth.token}`
                })
                console.log(data)
                await successStatusNotification(data.status)
            }
        } catch(e){}
    }

    const getIdFromPathname = () => {
        let tempId = window.location.pathname.replace(/\S*\//,'')
        if(tempId==='createGroup')
            return false
        else
        {
            setEditMod(true)
            return  parseInt(tempId)
        }
    }

    const fetchData = useCallback(async () => {

        try {
            const fetched = await request(`/training/${getIdFromPathname()}`, 'GET', null, {
                'X-Auth-Token': `${auth.token}`
            })
            console.log('geted pathname:', getIdFromPathname());
            console.log('fetched form from GET:',fetched)
            if(fetched.status===200)
            {
                await setForm(formTransform(fetched,formUIExample,'strict'))
                console.log('>>>>>>>>>>suka: ',formUIExample)
            }
            console.log('fetched formUI :',form)

        } catch (e) {}
    }, [request])

    useEffect(() => {
        fetchData()
    }, [fetchData])

    return (
        <>

            <div className="content-wrapper">
                <Navbar />
                <div className="content" id="tsp">
                <span>Заполните для создания тренировки</span>
                <span>Информация о вас появится автоматически</span>

                <InputField
                    typeName="datetime-local"
                    id='DOB'
                    outText="Дата и время проведения"
                    isDisabled={loading}
                    name="meet_date"
                    valueName={form.meet_date}
                    changeHandler={changeHandler}
                />

                <InputField
                    typeName="time"
                    id='duration'
                    outText="Продолжительность (мин)"
                    isDisabled={loading}
                    name="training_duration"
                    valueName={form.training_duration}
                    changeHandler={changeHandler}
                    step="1"
                />

                <SelectField
                    id="location"
                    isDisabled={loading}
                    name="location"
                    value={form.location}
                    innerText="Место провидения"
                    onChange={changeValueHandler}
                    options={[
                        'Автово',
                        'Невский Проспект',
                        'Пионерская',
                        'Горьковская'
                    ]}
                />

                <SelectField
                    id="person_sports"
                    innerText="Вид спорта"
                    isDisabled={loading}
                    name="sport"
                    value={form.sport}
                    onChange={changeValueHandler}
                    options={[
                        'футбол',
                        'атлетика',
                        'баскетбол',
                        'бег'
                    ]}
                />

                <SelectField
                    innerText="Требуемый уровень подготовки"
                    isDisabled={loading}
                    name="id_level"
                    value={form.id_level}
                    onChange={changeValueHandler}
                    options={[
                        'Не выбран',
                        'Начинающий',
                        'Продвинутый',
                        'Мастер',
                    ]}
                />

                <InputField
                    name='fee'
                    innerText="Стоимость участия(Руб)"
                    mask="99999"
                    maskPlaceholder=" "
                    isDisabled={loading}
                    valueName={form.fee}
                    changeHandler={changeHandler}
                />

                <InputField
                    name='comment'
                    innerText="Комментарий"
                    isDisabled={loading}
                    valueName={form.comment}
                    changeHandler={changeHandler}
                />

                <>
                    {
                        editMod &&
                        <Button
                            id="change-btn"
                            className="btn"
                            innerText="Редактировать тренировку"
                            disabled={loading}
                            onClick={() => saveBtnHandler(editMod)}
                        >
                        </Button>
                    }
                    {
                        !editMod &&
                        <Button
                            id="change-btn"
                            className="btn"
                            innerText="Создать тренировку"
                            disabled={loading}
                            onClick={() => saveBtnHandler(editMod)}
                        >
                        </Button>
                    }
                </>

            </div>
            </div>
        </>

    )
}
