import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {RegPage} from './pages/RegPage'
import {AuthPage} from './pages/AuthPage'
import {ProfilePage} from './pages/ProfilePage'
import {MessengerPage} from './pages/MessengerPage'
import {TrainingSearchPage} from './pages/TrainingSearchPage'
import {GroupTrainSearchPage} from './pages/GroupTrainSearchPage'
import {CreatingGroupTraining} from './pages/CreatingGroupTrainingPage'
import {CalendarPage} from './pages/CalendarPage'
import {MainMenuPage} from "./pages/MainMenuPage";
import {MapPage} from "./pages/MapPage";
import {WorkIsUnderwayPage} from "./pages/WorkIsUnderwayPage";

export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Switch>
                <Route path="/profile" component={ProfilePage}/>
                <Route path="/messenger"  component={WorkIsUnderwayPage}/>
                <Route path="/search"  component={GroupTrainSearchPage}/>
                <Route path="/searchGroup"  component={GroupTrainSearchPage}/>
                <Route path="/createGroup"  component={CreatingGroupTraining}/>
                <Route path="/calendar"  component={CalendarPage}/>
                <Route path="/menu"  component={MainMenuPage}/>
                <Route path="/map"  component={MapPage}/>
                <Redirect to="/menu" />
            </Switch>
        )
    }
    else
        return (
            <Switch>
                <Route exact path="/" component={AuthPage}/>
                <Route exact path="/sign-up" component={RegPage}/>
                <Redirect to="/" />
            </Switch>
        )
}
